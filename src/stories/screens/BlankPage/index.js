import * as React from "react";
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import { TextInput, Alert, ScrollView, Platform, StatusBar } from "react-native"
import { Dropdown } from 'react-native-material-dropdown'
import { View, Container, Header, Title, Content, Text, Button, Left, Icon, Right, Body} from "native-base"
import RadioButtonOption from '../../../theme/components/RadioForm'
import {JobDate} from '../../../theme/components/DatePicker'
import {addJob} from '../../../container/BlankPageContainer/action'

import styles from "./styles"



// const radio = new MDCRadio(document.querySelector('.mdc-radio'))
// const formField = new MDCFormField(document.querySelector('.mdc-form-field'))
// formField.input = radio;

export interface Props {
	navigation: any;
}
export interface State {}


class BlankPage extends React.Component<Props, State> {

	constructor(props) {
		super(props)
		this.state = { 
		text: '',
		description: '',
		category: 'category',
		start: ' ',
		end:' ',
		 }
	  }
	
	delete = () => {
		Alert.alert(
			'Are you sure you want to discard this form?',
			'Any unsaved progress will be deleted ',
			[
			  {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
			  {text: 'OK', onPress: () => this.props.navigation.goBack()} ,
			],
			{ cancelable: false }
		  )
	}
	

	comfirm = () => {
		console.log(this.state.category)

		let newJob = {
			"title" : this.state.text,
            "category" : this.state.category,
			"shortcut": "EX",
			"start": this.state.date,
			"description": this.state.description,
		}
		this.props.confirm(newJob);
		this.props.navigation.goBack()
		
	}
		TextInput: any;

	render() {
		const param = this.props.navigation.state.params;
		let dropdown1 = [{
			value: 'Wellington',
		  	}, {
			value: 'Auckland',
		  	}, {
			value: 'Christchurch',
			}]

		let dropdown2 = [{
			value: 'Labouring',
			}, {
			value: 'IT',
			}, {
			value: 'Production',
			}]
		
		// default value!
		if(param.name == undefined)
			param.name = "whatever"
		// ... jobType
		// ...  todo ...

		return (
			<Container style={{flex: 4, backgroundColor:'#1b1616' }} >
				<Header style={{marginTop: Platform.OS === 'ios' ? 12 : 30, backgroundColor:'#1b1616', borderBottomColor: 'black'}}>
					<View>
						<StatusBar
							translucent
							barStyle="light-content"
						/>
					</View>
					<Left style={{flex: 1}}>
						<Button transparent onPress={this.delete}>
							<Icon style={{color:'#9D1B33', fontSize: 40}} name="close" />
							<Title style={{marginLeft: 20, color: "#FFFFFF"}}>Create Listing</Title>
						</Button>
					</Left>
					<Right>
						{/* shit, I can't get rid of the border, you do it */}
						<Button block Iconcenter onPress={this.comfirm} style={{padding: 10, width: 90, height: 37, marginTop: 7, backgroundColor: '#9D1B33',marginBottom: 15  }}>
							<Icon name='md-checkmark' style={{color:'white', fontSize: 28}}/>
							{/* Image doesn't have onPress, check https://facebook.github.io/react-native/docs/image */}
						</Button>
					</Right>
				</Header>
				<Content padder style={{backgroundColor:'#322E2E'}}>
					<Text style={{paddingTop: 10, color: 'white'}}>I'm creating a ...</Text>
					<View style={{paddingBottom: 10}}>
						<Text style={{ color:'white'}} >
							{param !== undefined ? param.name.item : "Job Type"}
						</Text>
							<RadioButtonOption 
								key='0' 
								borderWidth={4}
								radio_props={[
								{label: 'Fast Listing  ', value: 0 },
								{label: 'Detailed Listing', value: 1 }
							]} />
					</View>
					<View style={{flex: 8, margin: 0, flexDirection: 'column', justifyContent: 'center'}}>
						<View enableAutoAutomaticScroll={false} style={{flex: 4, marginBottom: 0}}>
							<TextInput 
								placeholder="Job Title"
								placeholderTextColor="#b2b2b2"
								borderRadius={5}
								style={{backgroundColor: 'white', height: 45}}
								onChangeText={(text) => this.setState({text})}
								value={this.state.text}
							/>
						</View>	
						<Dropdown
							inputContainerStyle={{ borderBottomColor: 'transparent' }}
							placeholder='All Regions'
							placeholderTextColor='#b2b2b2'
							borderRadius={5}
							style={{backgroundColor: 'white', height: Platform.OS === 'ios' ? 44:45}}
							dropdownOffset={{
								top: 25,
								left: 70,
							}}
							containerStyle={{
								marginBottom: 5,
								marginTop:0
							}}
							itemTextStyle={{
								borderColor: 'white',
							}}
							baseColor='#b2b2b2'
							textColor='black'
							// label='All Regions'
							data={dropdown1}
						/>
						<Dropdown
							inputContainerStyle={{ borderBottomColor: 'transparent' }}
							placeholder='All Categories'
							placeholderTextColor='#b2b2b2'
							borderRadius={5}
							style={{backgroundColor: 'white', height: Platform.OS === 'ios' ? 44:45, marginBottom: Platform.OS === 'ios' ? 6:30}}
							dropdownOffset={{
								top: Platform.OS === 'ios' ? 25:25,
								left: Platform.OS === 'ios' ? 70:0,
							}}
							baseColor='#b2b2b2'
							textColor='black'
							// label='All Categories'
							data={dropdown2}
							onChangeText={(category) => this.setState({category})}
						/>
					</View>
					<Container style={{flex: 2, marginTop: Platform.OS === 'ios' ? 25:0}}>
						<View style={{flex: 1.5}}> 
						{/* don't put any judgement here, doesn't look good!

						replace   	param !== undefined ? param.name.item : "Job Type"     to     param.jobType

						if you want default value , put them before   */}
							<Text style={{paddingBottom: 10, color:'white'}}>
								Job Type
							</Text>
							<RadioButtonOption key='0' radio_props={[
								{label: 'Casual  ', value: 3 },
								{label: 'Part Time   ', value: 4 },
								{label: 'Full Time', value: 5 }
							]} />
						</View>
						<View style={{paddingTop: 10}}>	
							<Text style={{paddingBottom: 10, color:'white'}}>
								Job Length
							</Text>
							<RadioButtonOption key='1' radio_props={[
								{label: 'One Off ', value: 6 },
								{label: 'Fixed Term', value: 7 },
								{label: 'Permanent', value: 8 }
							]}/>
						</View>
						<View>
							<Text style={{ margin: 5, color:'white'}}>
								Start Date
							</Text>
							<JobDate 
								onDateChange={(date) => {this.setState({start: date}) }} 
								maxDate={this.state.end} 
							/>
						</View>
						<View style={{marginTop: 5}}>
							<Text style={{margin: 5, color:'white'}}>
								End Date
							</Text>
							<JobDate 
								onDateChange={(date) => {this.setState({end: date}) }} 
								minDate={this.state.start}
							/>
						</View>
						<View style={{flex: 8, margin: 5}}>
							<Text style={{marginTop: 10, marginBottom: 5, color: 'white', fontSize: 17}}>Hourly Rate</Text>
							<TextInput style={{width: 120, height: 45, color: 'black', borderBottomColor: 'white', borderRadius: 5, backgroundColor: 'white' , borderBottomWidth: 1}} placeholder='Enter a number' placeholderTextColor='#b2b2b2' keyboardType='number-pad' enableAutoAutomaticScroll={false} />
							<Text style={{ marginTop: 10, marginBottom: 5, color: 'white', fontSize: 17}}>Hours</Text>
							<TextInput style={{width: 120, height: 45, color: 'black', borderBottomColor: 'white', borderRadius: 5, backgroundColor: 'white' , borderBottomWidth: 1}} placeholder='Enter a number' placeholderTextColor='#b2b2b2' keyboardType='number-pad' enableAutoAutomaticScroll={false} />
							<Text style={{marginTop: 10, marginBottom: 5, color: 'white', borderBottomColor: 'white', fontSize: 17}}>Job Description</Text>
							<ScrollView keyboardDismissMode='on-drag'  keyboardShouldPersistTaps='always' enableAutoAutomaticScroll={false} >
								<TextInput 
									placeholder="Job Description"
									placeholderTextColor="#b2b2b2"
									style={{backgroundColor: 'white', height: 100  }}
									borderRadius={5}
									multiline = {true}
									maxLength = {1000}
									scrollEnabled={true}
									numberOfLines = {10}
									autoCorrect={true}
									onChangeText={(description) => this.setState({description})}
									value={this.state.description}
								/>
							</ScrollView>
						</View>
						<View style={{ flex: 2, marginBottom: 20, flexDirection: 'row', justifyContent: 'center'}}>
							<Button 
								rounded iconLeft 
								onPress={this.delete} 
								style={{ width: 120, margin:10, backgroundColor: '#b2b2b2' }}
							>
								<Icon name='trash'/>
								<Text>
									Discard
								</Text>
							</Button>
							<Button 
								rounded 
								iconLeft 
								onPress={this.comfirm} style={{ width: 120, margin:10, backgroundColor: '#9D1B33'}}
							>
								<Icon name='add' />
								<Text>
									Submit
								</Text>
							</Button>
						</View>
					</Container>
				</Content>
			</Container>
		)
	}
}

BlankPage = connect(mapStateToProps, mapDispatchToProps)(BlankPage)

function mapDispatchToProps(dispatch) {
	return { confirm: bindActionCreators(addJob, dispatch)}
}

function mapStateToProps (){
	return {
		test: '',
	}
}

export default BlankPage
