import * as React from "react";
import { Platform, Image, Linking, StatusBar } from "react-native";
import { Container, Content, Header, Body, Button, Text, View } from "native-base";
//import styles from "./styles";
export interface Props {
	loginForm: any,
	onLogin: Function,
}

export interface State {}
class Login extends React.Component<Props, State> {
	render() {
		return (
			<Container style={{flex: 1,flexDirection: 'column', justifyContent: 'center', alignSelf: 'center', width:800,backgroundColor:"#1B1616"}}>
				<Header style={{ marginTop: Platform.OS === 'ios' ? 12 : 12, backgroundColor:"#1B1616", borderBottomColor: '#1B1616'}}>
				<View>
					<StatusBar
						translucent
						backgroundColor='black'
						barStyle="light-content"
					/>
				</View>
				</Header>
					<Container style={{marginBottom: 20}}>
						<Body>
							<Content>
								<View style={{ marginBottom: 50, width: 200, flex: 1, flexDirection: 'column', justifyContent: 'center', alignSelf: 'center'}}>
									<View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignSelf: 'center'}}>
										<Image style={{paddingTop: 40,backgroundColor:"#1B1616", width: 200, height: 150, marginLeft: 16}} source={require('../../../../assets/label.png')} />
									</View>
									<View style={{marginTop: 20}}>	  
										{this.props.loginForm}
									</View>	 
								</View>
										<View style={{flex: 1, flexDirection: 'column',alignSelf: 'center'}}>
											<Button rounded block color="#1B1616" style={{ width: 290, height: 40, margin: 10, backgroundColor:'white'}} onPress={() => this.props.onLogin()}>
												<Text style={{color: '#9D1B33'}}>Sign in</Text>
											</Button>
											<Button 
												onPress={ () =>{ Linking.openURL('https://www.sjs.co.nz/')}}
												bordered
												rounded 
												block 
												color="#1B1616" style={{width: 290, height: 40, margin: 10, borderColor: '#9D1B33'}}>
												<Text style={{color: 'white'}}>Register</Text>
											</Button>
										</View>
							</Content>
						</Body>
					</Container>
			</Container>
		);
	}
}

export default Login;
