import React, { Component } from 'react'
import Slider from "react-native-slider"
import { View } from 'react-native'
import { AppRegistry, StyleSheet, Text } from "react-native"


class SlideBar extends Component {

    state = {
        value: 0.2
    }

    render() {
        const { slide_title, slide_props} = this.props

        return (
            <View style={styles.container}>
                <Text style={styles.slideValue}>
                    {slide_title}
                    Value: {this.state.value}
                </Text>
                <Slider
                    value={this.state.value}
                    onValueChange={value => this.setState({ value })}
                />
          </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 40,
    },
    slideValue: {
      color: '#9D1B33',
      fontSize: 20,
    }
  });

export default SlideBar;