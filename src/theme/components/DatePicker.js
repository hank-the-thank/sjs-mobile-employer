import React, {
  Component
} from 'react'
import DatePicker from 'react-native-datepicker'
import {
  StyleSheet
} from "react-native"


var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

export class JobDate extends Component {
  constructor(props) {
    super(props)
    this.state = {
      date: yyyy+'-'+mm+'-'+dd
    }
  }

  changed = (date) => {
    this.props.onDateChange(date)
    this.setState({
      date
    })
  }

  render() {

    return ( <
      DatePicker style = {
        styles.date
      }
      date = {
        this.state.date
      }
      mode = "date"
      placeholder = "select date"
      format = "YYYY-MM-DD"
      minDate = {this.props.minDate}
      maxDate = {this.props.maxDate}
      confirmBtnText = "Confirm"
      cancelBtnText = "Cancel"
      customStyles = {
        {
          dateIcon: {
            position: 'absolute',
            left: 0,
            right: 10,
            top: 5,
            marginLeft: 0,
            
          },
          dateInput: {
            marginLeft: 36,
            backgroundColor: 'white',
            borderRadius:5
          },
          placeholderText: {
            fontSize: 18,
          },
          dateInputBody: {
            backgroundColor: 'white'
          },
          dateText: {
            color: 'black'
          },
          btnTextConfirm: {
            color: '#9D1B33'
          },
        }
      }
      onDateChange = {
        this.changed
      }
      />
    )
  }
}


const styles = StyleSheet.create({
  date: {
    width: 300,
    marginLeft: 40,
  }
})