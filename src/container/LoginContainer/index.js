// @flow
import * as React from "react";
import {Linking} from 'react-native'
import { Item, Input, Toast, Form, Text } from "native-base";
import { Field, reduxForm } from "redux-form";
import Login from "../../stories/screens/Login";

const required = value => (value ? undefined : "Required");
const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined;
const maxLength15 = maxLength(15);
const minLength = min => value =>
  value && value.length < min ? `Must be ${min} characters or more` : undefined;
const minLength8 = minLength(8);
const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? "Invalid email address"
    : undefined;
const alphaNumeric = value =>
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? "Only alphanumeric characters"
    : undefined;

export interface Props {
  navigation: any;
}
export interface State {}
class LoginForm extends React.Component<Props, State> {
  textInput: any;

  renderInput({ input, label, type, meta: { touched, error, warning } }) {
    return (
      <Item error={error && touched}>
        {/* <Icon active name={input.name === "email" ? "person" : "unlock"} /> */}
        <Input
          style={{color:"white", marginTop: 25}}
          ref={c => (this.textInput = c)}
          placeholder={input.name === "email" ? "Email" : "Password"}
          secureTextEntry={input.name === "password" ? true : false}
          {...input}
        />
      </Item>
    );
  }

  login() {
    if (this.props.valid) {
      this.props.navigation.navigate("Drawer");
    } else {
      Toast.show({
        text: "Enter Valid Username & password!",
        duration: 2000,
        position: "top",
        textStyle: { textAlign: "center" }
      });
    }
  }

  render() {
    const form = (
          <Form style={{borderTopColor: '#2E2E32', alignSelf:'center', width: 300, height: 180, position: 'relative'}}>
            <Field
              style={{borderBottomColor:"white", borderBottomWidth: 8, placeholderColor: "white"}}
              name="email"
              component={this.renderInput}
              validate={[email, required]}
            />
            <Field
              style={{borderBottomColor:"white", borderBottomWidth: 8}}
              name="password"
              component={this.renderInput}
              validate={[alphaNumeric, minLength8, maxLength15, required]}
            />
            <Text onPress={ () =>{ Linking.openURL('https://www.sjs.co.nz/')}} 
                style={{color: '#B2B2B2', marginTop: 5, textAlign: 'right', fontSize: 13}}>
                Forgot sign in details
            </Text>
          </Form>
    );
    return (
      <Login
        navigation={this.props.navigation}
        loginForm={form}
        onLogin={() => this.login()}
      />
    );
  }
}
const LoginContainer = reduxForm({
  form: "login"
})(LoginForm);
export default LoginContainer;
